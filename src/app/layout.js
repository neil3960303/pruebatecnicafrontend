import { Inter } from "next/font/google";
import "./globals.css";
import { AppRouterCacheProvider } from "@mui/material-nextjs/v14-appRouter";
import Navbar from '@/components/Navbar'
import { NoticiaProvider } from "@/context/NoticiasContext";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "El Hocicon",
  description: "Neil Graneros Flores",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <Navbar/>
        <NoticiaProvider>
          <AppRouterCacheProvider><div className="container-main">{children}</div></AppRouterCacheProvider>
        </NoticiaProvider>
      </body>
    </html>
  );
}
