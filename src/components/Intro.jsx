
import Divider from '@mui/material/Divider';

const Intro = () => {
  return (
    <div style={{lineHeight:"20px", marginTop:"150px"}}>
      <h2 style={{fontSize:"50px", color:"#424769", marginBottom:"0"} } className="titulo">El Hocicón:</h2>{" "}
      <h3 style={{fontWeight:"300"}}>
        Donde las noticias cobran vida, ¡lamemos juntos la actualidad y descubre
        el sabor fresco de la información!
      </h3>
      <Divider sx={{ bgcolor: "black.ligth", borderWidth:"1px"  }}/>
    </div>
  );
};

export default Intro;
