"use client";
import { useNoticias } from "@/context/NoticiasContext";
import FormNoticias from "@/components/FormNoticias";
import { useRouter } from "next/navigation";
import { PostNoticias } from "@/services/api";
import { Button, Divider, Typography } from "@mui/material";

const AddNoticia = () => {
  const value = useNoticias();
  const router = useRouter();
  const initialValues = {
    titulo: "",
    fechaPubli: "",
    lugar: "",
    autor: "",
    contenido: "",
  };
  const crearNoticia = async (noticia) => {
    try {
      await PostNoticias(noticia);
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <div className="containerNewNoticia">
      <Typography
        variant="h1"
        sx={{ fontSize: "30px", marginBottom: "15px", color: "#424769" }}
      >
        Crear una noticia
      </Typography>
      <Divider
        sx={{
          bgcolor: "black.ligth",
          borderWidth: "1px",
          marginBottom: "15px",
        }}
      />
      <Button
        onClick={() => router.push("/")}
        variant="contained"
        sx={{ marginBottom: "15px" }}
      >
        Atras
      </Button>
      <Divider
        sx={{
          bgcolor: "black.ligth",
          borderWidth: "1px",
          marginBottom: "15px",
        }}
      />
      <div className="containerFormulario">
        <div className="formulario">
          <FormNoticias
            initialValuesNoticias={initialValues}
            tituloToast={"creada"}
            fetchRes={crearNoticia}
            link={"/"}
          />
        </div>
      </div>
    </div>
  );
};

export default AddNoticia;
