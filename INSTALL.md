# Guía de Instalación para la Aplicación Frontend en Next.js
----
### Instalamos los paquetes y dependencias necesarias:
```bash
npm i
````
### Realizamos el build
```bash
npm run build
````
### Iniciar la Aplicación:
Finalmente, puedes iniciar la aplicación Next.js con el siguiente comando:
```bash
npm start
````
La aplicación se ejecutará en `localhost:3000`
### Recomendaciones:
- Tener instalado Node JS
- Debes de ejecutar primeramente el backend para que evitar problema de ejecución.
- Verifica que todas las dependencias estén instaladas utilizando `npm install` o `npm i` antes de ejecutar los comandos.
- Asegúrate de tener Docker instalado y en funcionamiento antes de levantar la base de datos.