"use client";
import { fetchNoticia } from "@/services/api";
import { notFound } from "next/navigation";
import { useRouter } from "next/navigation";
import DeleteNoticia from "@/components/DeleteNoticia";
import { Button, Divider } from "@mui/material";
import Typography from "@mui/material/Typography";
import { Image as ImgVec } from "@/components/Image";

const Noticia = async ({ params }) => {
  const router = useRouter();

  const noticia = await fetchNoticia(params.id);
  if (!noticia) notFound();

  const funEditNoticia = (id) => {
    router.push(`/noticias/edit/${id}`);
  };
  const indiceAleatorio = Math.floor(Math.random() * ImgVec.length);
  return (
    <>
      <div className="containerNoticia">
        <Button onClick={() => router.push("/")} variant="contained">
          Atras
        </Button>
        <h2>Noticia</h2>
        <Divider
          sx={{
            bgcolor: "black.ligth",
            borderWidth: "1px",
            marginBottom: "15px",
          }}
        />
        <div className="botonOpcionesNoticia">
          <Button
            onClick={() => funEditNoticia(noticia.id)}
            variant="contained"
            color="success"
          >
            editar
          </Button>
          <DeleteNoticia idElim={noticia.id} />
        </div>
        <Divider
          sx={{
            bgcolor: "black.ligth",
            borderWidth: "1px",
            marginBottom: "15px",
          }}
        />

        <div>
          <Typography
            gutterBottom
            variant="h1"
            sx={{ fontSize: "30px", marginBottom: "15px" }}
          >
            {noticia.titulo}
          </Typography>
          <Typography
            variant="h2"
            className="fontNormal"
            sx={{ fontSize: "18px", textAlign: "right", fontWeight: "300" }}
          >
            Autor: {noticia.autor}
          </Typography>
          <Typography
            variant="h3"
            className="fontNormal"
            sx={{ fontSize: "18px", textAlign: "right", fontWeight: "300" }}
          >
            Lugar: {noticia.lugar}
          </Typography>
          <Typography
            variant="h2"
            sx={{
              fontSize: "13px",
              textAlign: "right",
              fontStyle: "italic",
              marginTop: "4px",
              marginBottom: "20px",
            }}
          >
            Fecha: {noticia.fechaPubli}
          </Typography>
          <div className="containerFotoNoticia">
            <img src={ImgVec[indiceAleatorio]} alt="" className="noticiaFoto" />
          </div>
          <Typography
            variant="p"
            sx={{
              fontSize: "15px",
              textAlign: "justify",
              marginTop: "4px",
              marginBottom: "20px",
            }}
          >
            {noticia.contenido}
          </Typography>
        </div>
      </div>
    </>
  );
};

export default Noticia;
