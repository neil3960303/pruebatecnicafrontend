"use client";
import { createContext, useContext } from "react";
export const NoticiaContext = createContext();
export const useNoticias = () => {
  const context = useContext(NoticiaContext);
  if (!context)
    throw new Error("useNoticias deberia estar dentro de un Provider");
  return context;
};
export const NoticiaProvider = ({ children }) => {
  const r = "hola";
  return (
    <NoticiaContext.Provider value={r}>{children}</NoticiaContext.Provider>
  );
};
