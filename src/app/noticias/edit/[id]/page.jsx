"use client";
import React, { useState, useEffect } from "react";
import FormNoticia from "@/components/FormNoticias";
import { PatchNoticia, fetchNoticia } from "@/services/api";
import { Button, Divider, Typography } from "@mui/material";
import { useRouter } from "next/navigation";

const Editar = ({ params }) => {
  const [noticiaValue, setNoticiaValue] = useState({});
  const [idNoticia, setIdNoticia] = useState(null);
  const router = useRouter();
  const atras=()=>{router.push(`/noticias/${idNoticia}`)}
  useEffect(() => {
    obtenerNoticia(params.id);
  }, []);

  const obtenerNoticia = async (id) => {
    try {
      const noticia = await fetchNoticia(id);
      if (!noticia) notFound();
      setNoticiaValue({
        titulo: noticia.titulo,
        fechaPubli: noticia.fechaPubli,
        lugar: noticia.lugar,
        autor: noticia.autor,
        contenido: noticia.contenido,
      });
      setIdNoticia(noticia.id);
    } catch (error) {
      console.error(error);
    }
  };

  const editarNoticia = async (noticia) => {
    try {
      await PatchNoticia(idNoticia, noticia);
    } catch (error) {
      console.error(error);
    }
  };
  return (
    <div className="containerNewNoticia">
      <Typography
        variant="h1"
        sx={{ fontSize: "30px", marginBottom: "15px", color: "#424769" }}
      >
        Editar una noticia
      </Typography>
      <Divider
        sx={{
          bgcolor: "black.ligth",
          borderWidth: "1px",
          marginBottom: "15px",
        }}
      />
      <Button
        onClick={atras}
        variant="contained"
        sx={{ marginBottom: "15px" }}
      >
        Atras
      </Button>
      <Divider
        sx={{
          bgcolor: "black.ligth",
          borderWidth: "1px",
          marginBottom: "15px",
        }}
      />
      <div className="containerFormulario">
        <div className="formulario">
          <FormNoticia
            fetchRes={editarNoticia}
            initialValuesNoticias={noticiaValue}
            tituloToast={"editada"}
            link={`/noticias/${idNoticia}`}
          />
        </div>
      </div>
    </div>
  );
};

export default Editar;
