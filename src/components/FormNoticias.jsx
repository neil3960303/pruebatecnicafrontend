import React, { useEffect, useState } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { useRouter } from "next/navigation";
import toast, { Toaster } from "react-hot-toast";
import Textarea from "@mui/joy/Textarea";
import { Typography } from "@mui/material";

const FormNoticias = ({
  initialValuesNoticias,
  fetchRes,
  tituloToast,
  link,
}) => {
  const router = useRouter();
  const [initialValues, setInitialValues] = useState({
    titulo: "",
    fechaPubli: "",
    lugar: "",
    autor: "",
    contenido: "",
  });

  useEffect(() => {
    setInitialValues(initialValuesNoticias);
  }, [initialValuesNoticias]);

  const noticiaSchema = yup.object().shape({
    titulo: yup.string().required("El título es obligatorio"),
    fechaPubli: yup.date().required("La fecha de publicación es obligatoria"),
    lugar: yup.string().required("El lugar es obligatorio"),
    autor: yup.string().matches(/^[a-zA-Z\s]+$/, 'No debe contener números').required("El autor es obligatorio"),
    contenido: yup.string().required("El contenido es obligatorio"),
  });
  const agregar = async (values, actions) => {
    try {
      await fetchRes(values);
      toast.success(`Noticia ${tituloToast} exitosamente`);
      actions.resetForm();
      setTimeout(() => {
        router.push(link);
      }, 1250);
    } catch (error) {
      console.error(error);
      toast.error("Upss vuelce a intentarlo.");
    }
  };
  return (
    <>
      <Toaster position="top-center" reverseOrder={true} />
      <Formik
        enableReinitialize
        initialValues={initialValues}
        validationSchema={noticiaSchema}
        onSubmit={agregar}
      >
        {({
          values,
          errors,
          handleChange,
          handleSubmit = (values) => agregar(values),
          handleBlur,
          touched,
          isValidating,
          isValid,
          isSubmitting,
        }) => (
          <>
            <Form onSubmit={handleSubmit} className="formNoticia">
              <TextField
                fullWidth
                id="titulo"
                name="titulo"
                label="Titulo"
                value={values?.titulo}
                onChange={handleChange}
                onBlur={handleBlur}
                error={touched.titulo && Boolean(errors.titulo)}
                helperText={touched.titulo && errors.titulo}
                sx={{ marginBottom: "10px" }}
              />
              <div>
                <Typography
                  variant="span"
                  sx={{
                    fontSize: "15px",
                    marginBottom: "5px",
                    color: "#424769",
                    textAlign: "left",
                    fontWeight: "bold",
                  }}
                >
                  Fecha:
                </Typography>
                <Field
                  type="date"
                  name="fechaPubli"
                  className="date"
                  value={values?.fechaPubli}
                />
              </div>
              <ErrorMessage
                name="fechaPubli"
                component="p"
                className={`error-message-style ${
                  errors?.fechaPubli && touched.fechaPubli ? "error-border" : ""
                }`}
              />
              <TextField
                fullWidth
                id="autor"
                name="autor"
                label="autor"
                value={values?.autor}
                onChange={handleChange}
                onBlur={handleBlur}
                error={touched.autor && Boolean(errors.autor)}
                helperText={touched.autor && errors.autor}
                sx={{ marginBottom: "10px" }}
              />
              <TextField
                fullWidth
                id="lugar"
                name="lugar"
                label="Lugar"
                value={values?.lugar}
                onChange={handleChange}
                onBlur={handleBlur}
                error={touched.lugar && Boolean(errors.lugar)}
                helperText={touched.lugar && errors.lugar}
                sx={{ marginBottom: "10px" }}
              />
              <Typography
                variant="span"
                sx={{
                  fontSize: "15px",
                  marginBottom: "5px",
                  color: "#424769",
                  textAlign: "left",
                  width: "100%",
                  fontWeight: "bold",
                }}
              >
                Contenido:
              </Typography>
              <Textarea
                minRows={5}
                size="lg"
                id="contenido"
                name="contenido"
                label="contenido"
                value={values?.contenido}
                onChange={handleChange}
                onBlur={handleBlur}
                error={touched.contenido && Boolean(errors.contenido)}
                helperText={touched.contenido && errors.contenido}
                sx={{ marginBottom: "15px", width: "100%" }}
              />
              <div className="errorForm">
                {isValidating || !isValid
                  ? "Revisa si introduciste bien los campos"
                  : ""}
              </div>
              <Button
                color="success"
                variant="contained"
                disabled={isValidating || !isValid || isSubmitting}
                fullWidth
                type="submit"
              >
                {isSubmitting ? "Enviando" : "Enviar"}
              </Button>
            </Form>
          </>
        )}
      </Formik>
    </>
  );
};

export default FormNoticias;
