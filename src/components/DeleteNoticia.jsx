"use client";
import * as React from "react";
import { useRouter } from "next/navigation";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import {DeleteNoticia} from '@/services/api'
import toast, { Toaster } from "react-hot-toast";


const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid rgba(0,0,0,0.18)",
  borderRadius: "10px",
  boxShadow: 24,
  p: 4,
};
const DeletedNoticia = ({idElim}) => {
    const router = useRouter();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const eliminarNoticia = async () => {
    try {
      await DeleteNoticia(idElim);
      toast.error("Noticia Eliminada.")
      setTimeout(() => {
        router.push(`/`);
      }, 1050);

    } catch (error) {
      console.error(error);
      toast.error("Upss error, vuelve a inentarlo.")
    }
  };
  return (
    <div>
        <Toaster position="top-center" reverseOrder={true} />
      <Button onClick={handleOpen} variant="contained" color="warning" sx={{marginLeft:"10px"}}>Eliminar</Button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Typography
              id="transition-modal-title"
              variant="h6"
              component="h2"
              textAlign={"center"}
              color={"rgb(225 29 72)"}
              fontFamily={"Poppins"}
              fontSize={"30px"}
            >
              Eliminar Noticia
            </Typography>
            <div style={{ textAlign: "center" }}>
              <svg
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                stroke="E	#E11D48"
                width={60}
                style={{ display: "block", margin: "auto" }}
              >
                <g id="SVGRepo_bgCarrier" strokeWidth="0"></g>
                <g
                  id="SVGRepo_tracerCarrier"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                ></g>
                <g id="SVGRepo_iconCarrier">
                  {" "}
                  <path
                    d="M12 15H12.01M12 12V9M4.98207 19H19.0179C20.5615 19 21.5233 17.3256 20.7455 15.9923L13.7276 3.96153C12.9558 2.63852 11.0442 2.63852 10.2724 3.96153L3.25452 15.9923C2.47675 17.3256 3.43849 19 4.98207 19Z"
                    stroke="#E11D48"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  ></path>{" "}
                </g>
              </svg>
            </div>

            <Typography
              id="transition-modal-description"
              sx={{ mt: 2 }}
              textAlign={"center"}
              fontFamily={"Poppins"}
              marginBottom={"30px"}
            >
              Desea eliminar la Noticia seleccionada?
            </Typography>
            <Button
              fullWidth
              color="warning"
              variant="contained"
              style={{ fontFamily: "'Poppins', sans-serif", fontSize: "20px" }}
              onClick={()=>eliminarNoticia()}
            >
              Eliminar
            </Button>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default DeletedNoticia;
