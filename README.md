# Frontend - Prueba Técnica

##### **Nombre del Postulante:** Neil Ángel Graneros Flores
----
## Descripción del Proyecto:

Este proyecto aborda las consignas proporcionadas utilizando las siguientes tecnologías:

- Node.js 21.5
- Next.js 14.0.4
- MUI 5.15.3

El proyecto fontend desempeña las funciones de interfaz de ususario que se pidieron. Para petcion de CRUD se realizo una vista que ayuda a la visualización de los datos que manda el servidor backend. El proyecto está desarrollado en Next.js.

----
### Respecto al Proyecto
El proyecto muestra las vistas para las peticiones CRUD con el servidor, en cuanto las imágenes estas son eleatorias y solo son elementos graficos.

----
### Mi opinión al realizar el proyecto
En cuanto a mi opinión sobre la práctica, la considero un reto valioso, ya que nunca había trabajado con esta tecnología. Fue enriquecedor, ya que pude comprender la estructura de como contruye una aplicación para el lado del cliente. Ya que se basa en React  pude comprenderlo mucho mejor por mi breve conocimiento en esta tecnología, Next JS es un frameworl intersante y de hecho, seguiré aprendiendolo. Agradezco la oportunidad, ¡Buen día!

----
### Contacto:
correo: neilgraneros11@gmail.com    
cel: 60160883