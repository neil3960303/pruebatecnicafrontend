import { NextResponse } from "next/server";

const url_base = "http://127.0.0.1:4000/";

export const fetchNoticias = async (q = "", page = 1, limit = 10) => {
  const res = await fetch(
    url_base + `noticias/?q=${q}&page=${page}&limit=${limit}`
  );
  const data = await res.json();
  return data;
};
export const fetchNoticiasFecha = async (page = 1, limit = 10) => {
  const res = await fetch(
    url_base + `noticias/fecha?&page=${page}&limit=${limit}`
  );
  const data = await res.json();
  return data;
};
export const fetchNoticia = async (id) => {
  const res = await fetch(url_base + `noticias/${id}`);
  const data = await res.json();
  return data;
};
//post
export const PostNoticias = async (values) => {
  try {
    const res = await fetch(url_base + `noticias/`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(values),
    });

    if (res.ok) {
      return res;
    } else {
      const errorResponse = await res.json();
      throw new Error(`Error en la solicitud POST: ${errorResponse.message}`);
    }
  } catch (error) {
    console.error(error);
    throw error;
  }
};
//patch
export const PatchNoticia = async (id, noticia) => {
  try {
    const res = await fetch(url_base + `noticias/${id}`, {
      method: "PATCH",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(noticia),
    });

    if (res.ok) {
      return res;
    } else {
      const errorResponse = await res.json();
      throw new Error(`Error en la solicitud PATCH: ${errorResponse.message}`);
    }
  } catch (error) {
    console.error(error);
    throw error;
  }
};
//delete
export const DeleteNoticia = async (id) => {
  try {
    const res = await fetch(url_base + `noticias/${id}`, {
      method: "DELETE",
    });

    if (res.ok) {
      return res;
    } else {
      const errorResponse = await res.json();
      throw new Error(`Error en la solicitud DELETE: ${errorResponse.message}`);
    }
  } catch (error) {
    console.error(error);
    throw error;
  }
};