import { Pagination, Stack } from "@mui/material";
import React, { useState } from "react";
import { useRouter } from "next/navigation";
import Card from "@/components/Card";
import { Image as ImgVec } from "@/components/Image";

const Noticias = ({ data, fetchData, total, page, setPage }) => {
  const [itemsPorPage, setItemsPorPage] = useState(10);
  const router = useRouter();

  const handlePageChange = (_, newPage) => {
    fetchData(newPage);
    setPage(newPage);
  };
  const handleNoticia = (idVer) => {
    router.push(`/noticias/${idVer}`);
  };
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        justifyContent: "center",
      }}
    >
      {data ? (
        <>
          <div className="containerCards">
            {data.map((noticia, index) => {
              const indiceAleatorio = Math.floor(Math.random() * ImgVec.length);

              return (
                <div key={noticia.id}>
                  <Card
                    titulo={noticia.titulo}
                    fecha={noticia.fechaPubli}
                    contenido={noticia.contenido}
                    imagen={ImgVec[indiceAleatorio]}
                    ver={handleNoticia}
                    id={noticia.id}
                  />
                </div>
              );
            })}
          </div>
          <div className="container-pagination">
            <Stack spacing={2}>
              <Pagination
                count={Math.ceil(total / itemsPorPage)}
                page={page}
                onChange={handlePageChange}
                color="primary"
                sx={{ padding: "10px" }}
              />
            </Stack>
          </div>
        </>
      ) : (
        <p>No hay noticias disponibles.</p>
      )}
    </div>
  );
};

export default Noticias;
