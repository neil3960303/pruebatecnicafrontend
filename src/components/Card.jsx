import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button, CardActionArea, CardActions } from "@mui/material";

const CardNoticia = ({imagen,titulo, fecha, contenido,ver,id}) => {
  return (
    <Card sx={{ maxWidth: 345, maxHeight:350 }}>
      <CardActionArea>
        <CardMedia
          component="img"
          height="160"
          image={imagen}
          alt="green iguana"
          width="100"
        />
        
        <CardContent>
          <Typography gutterBottom variant="h5" component="div" sx={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}>
            {titulo}
          </Typography>
          <Typography variant="p" component="div" align="right" className="font-normal" fontSize={"12px"} color={"#3887BE"}>Id:{id}</Typography>
          <Typography gutterBottom variant="p" component="div" align="right" className="font-normal" fontSize={"13px"}color={"#3887BE"}>
           Fecha: {fecha}
          </Typography>
          <Typography variant="body2" color="text.secondary" sx={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}>
            {contenido}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" variant="contained" onClick={()=>ver(id)} sx={{width:"100%", bgcolor:"#176B87"} }>
          Ver noticia
        </Button>
      </CardActions>
    </Card>
  );
};

export default CardNoticia;
