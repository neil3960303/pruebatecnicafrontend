"use client";
import { useEffect, useState } from "react";
import { fetchNoticias, fetchNoticiasFecha } from "@/services/api";
import { useRouter } from "next/navigation";
import Noticias from "@/components/Noticias";
import Intro from "@/components/Intro";
import { Button } from "@mui/material";

const Noticia = () => {
  const router = useRouter();

  const [noticias, setNoticias] = useState([]);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [q, setq] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getNoticias();
  }, [q]);
  const getNoticias = async (nroPage = 1, limit = 10) => {
    setPage(nroPage);
    const res = await fetchNoticias(q, nroPage, limit);

    setTotal(res.total);
    setNoticias(res.data);
    setLoading(false);
  };
  const getNoticiasFecha = async (nroPage = 1, limit = 10) => {
    setPage(nroPage);
    const res = await fetchNoticiasFecha(nroPage, limit);

    setTotal(res.total);
    setNoticias(res.data);
    setLoading(false);
  };
  const funBuscar = (e) => {
    setq(e.target.value);
  };
  const funAddNoticia = () => {
    router.push(`/noticias/new`);
  };

  return (
    <div>
      <Intro />
      <h1 style={{ color: "#0F1035" }}>Noticias</h1>

      <div className="div-search">
        <p className="fontSearch">
          Buscar por <b>Titulo</b>
        </p>
        <div className="containerSvg">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="svg-iconSearch"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
            />
          </svg>
          <input
            type="text"
            placeholder="Buscar"
            onChange={(e) => funBuscar(e)}
            className="input-search"
          />
        </div>
      </div>
      <div className="botonesInicio">
        <Button
          onClick={() => getNoticiasFecha()}
          variant="contained"
          sx={{ bgcolor: "#419197" }}
        >
          Ordenar por Fecha
        </Button>
        <Button onClick={funAddNoticia} variant="contained" color="success" sx={{ bgcolor: "#E8AA42" }} >
          Añadir noticia
        </Button>
      </div>

      {loading ? (
        <p>Cargando...</p>
      ) : (
        <Noticias
          data={noticias}
          fetchData={getNoticias}
          total={total}
          page={page}
          setPage={setPage}
        />
      )}
    </div>
  );
};

export default Noticia;
